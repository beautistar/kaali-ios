//
//  PhotoCommentCell.swift
//  Kaali
//
//  Created by Yin on 10/03/2018.
//  Copyright © 2018 Yin. All rights reserved.
//

import UIKit
import GrowingTextView

class PhotoCommentCell: UITableViewCell {

    @IBOutlet weak var imvphoto: UIImageView!
    @IBOutlet weak var imageHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var txvComment: GrowingTextView!
    //@IBOutlet weak var txvComment: UITextView!
    @IBOutlet weak var commentTextHeightConstraint: NSLayoutConstraint!
    
    var imageSize: CGSize?
    
    //var indexPath: IndexPath!
    var textViewGrowingDelegate: GrowingTextViewDelegate?
    var commentChangeHandler: ((PhotoCommentCell, String?) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        // *** Customize GrowingTextView ***
        txvComment.layer.cornerRadius = 4.0
        txvComment.placeholder = "Say something..."
        txvComment.minHeight = 80
        txvComment.maxHeight = 80
        
        txvComment.delegate = self
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension PhotoCommentCell: GrowingTextViewDelegate {

    // *** Call layoutIfNeeded on superview for animation when changing height ***

    func textViewDidChangeHeight(_ textView: GrowingTextView, height: CGFloat) {
        UIView.animate(withDuration: 0.3, delay: 0.0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.7, options: [.curveLinear], animations: { () -> Void in

            self.commentTextHeightConstraint.constant = height
            if let imageSize = self.imageSize {
                self.frame.size.height = UIScreen.main.bounds.width * imageSize.height / imageSize.width + height + 20
            }

            self.layoutIfNeeded()
            self.superview?.layoutIfNeeded()
        }, completion: nil)
    }

    func textViewDidChange(_ textView: UITextView) {
        commentChangeHandler?(self, textView.text)
    }
}
/*
protocol TextViewGrowingDelegate {
    func growingTextView()
}*/
