//
//  PhotoCell.swift
//  Kaali
//
//  Created by Yin on 09/03/2018.
//  Copyright © 2018 Yin. All rights reserved.
//

import UIKit

class PhotoCell: UICollectionViewCell {
    
    @IBOutlet weak var imvPhoto: UIImageView!
    @IBOutlet weak var imvCheck: UIImageView!
    @IBOutlet weak var hideView: UIView!
    
    
    override func draw(_ rect: CGRect) { //Your code should go here.
        super.draw(rect)
        imvCheck.layer.borderColor = UIColor.white.cgColor
    }    
}
