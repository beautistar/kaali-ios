//
//  ParseHelper.swift
//  Kaali
//
//  Created by Yin on 25/03/2018.
//  Copyright © 2018 Yin. All rights reserved.
//

import Foundation
import SwiftyJSON

class ParseHelper {
    
    static func parse_event(_ json: JSON) -> EventModel {        
        
        let event_id = json[Const.PARAM_EVENTID].stringValue
        let event_name = json[Const.PARAM_EVENTNAME].stringValue
        let event_password = json[Const.PARAM_EVENT_PASSWORD].stringValue
        let event_userid = json[Const.PARAM_USERID].intValue
        let is_approved = json[Const.PARAM_ISAPPOVED].intValue
    
        return EventModel.init(event_name: event_name,
                               event_id: event_id,
                               event_password: event_password,
                               event_userid: event_userid,
                               is_approved: is_approved)
    }
    
    static func parse_photo(_ json: JSON) -> PhotoModel {
        
        let photo = PhotoModel()
        
        photo.photo_id = json[Const.PARAM_PHOTOID].intValue
        photo.photo_url = json[Const.PARAM_PHOTOURL].stringValue
        photo.is_approved = json[Const.PARAM_ISAPPOVED].intValue
        return photo
    }
}

