//
//  Const.swift
//  Kaali
//
//  Created by Yin on 07/03/2018.
//  Copyright © 2018 Yin. All rights reserved.
//

import Foundation
import UIKit

var isCaptured: Bool = false
var isPaid: Bool = false


class Const {
    
    static let mainColor = UIColor.init(red: 3/255.0, green: 169/255.0, blue: 244/255.0, alpha: 1)
    
    static let STRIPE_PUBLISH_KEY           = "pk_test_wAp14O256Y9ktN87QJhwy6r7"
    static let PAYPAL_SANDBOX_KEY           = "Ac9XQqsvi6I0we1xRJVTUPE7E5XSYZVutwnYPWCzNEIajbGJXIXgY_IgcFQCcRMP7B-qWfMGB8IyvUEs"
    static let PAYPAL_PRODUCTION_KEY        = "AXkHejooAeib8E7sx4B1sSsEjyNi_NUZQWH574-zH53vnqoZZ6bTCf7fj5bdFKd8w_ohK-VbIg23DVoe"
    
    
    // Strings
    static let APP_NAME                     = "Kaali"
    static let SAVE_ROOT_PATH               = "Kaali"
    static let ERROR_CONNECT                = "Failed to server connection"
    static let LOGIN_SUCCESS                = "Login success"
    static let REGISTER_SUCCESS             = "Register success"
    static let LOGIN_FAILED                 = "Login failed\n please input correct ID and password"
    static let SUCCESS                      = "Success"
    static let UPLOAD_SUCCESS               = "Success to upload photo"
    static let UPLOAD_FAIL                  = "Failed to upload photo"
    
    static let ERROR                        = "Error"
    static let OK                           = "OK"
    
    
    static let INPUT_USERNAME               = "Please input username"
    static let MATCH_PASSWORD               = "Password does not match"
    static let INPUT_EVENTNAME              = "Please input event name"
    static let INPUT_EVENTID                = "Please input event ID"
    static let INPUT_PASSWORD               = "Please input password"
    static let AGREE_TERM                   = "Please agree to terms of service"    
    static let EXIST_EVENT                  = "Event ID is already registered"
    static let EXIST_USER                   = "Username already registered"
    static let ERROR_PAYPALPAY              = "You can't proceed payment now because of have some error."
    static let MSG_AWATING                  = "Awaiting approval"

    
    // Parameters
    static let PARAM_ID                     = "id"
    static let PARAM_USERID                 = "user_id"
    static let PARAM_EMAIL                  = "email"
    static let PARAM_USERNAME               = "user_name"
    static let PARAM_FIRSTNAME              = "first_name"
    static let PARAM_LASTNAME               = "last_name"
    static let PARAM_PASSWORD               = "password"
    static let PARAM_TOKEN                  = "token"
    
    static let PARAM_EVENTNAME              = "event_name"
    static let PARAM_EVENTID                = "event_id"
    static let PARAM_EVENT_PASSWORD         = "event_password"
    static let PARAM_ISAPPOVED              = "is_approved"
    static let PARAM_EVENTLIST              = "event_list"
    
    static let PARAM_EVENT_PHOTOS           = "event_photos"
    static let PARAM_PHOTOID                = "photo_id"
    static let PARAM_PHOTOURL               = "photo_url"
    static let PARAM_COMMENT                = "comment"
    static let PARAM_ORDERLIST              = "order_list"
    static let PARAM_TRANSACTIONID          = "transaction_id"
    
    static let PARAM_STRIPETOKEN            = "stripe_token"
    static let PARAM_AMOUNT                 = "amount"
    static let PARAM_MESSAGE                = "message"
    static let PARAM_PRICE                  = "price"
    static let PARAM_SERVICEFEE             = "service_fee"
    
    // Key
    static let KEY_USERID                   = "k_userid"
    static let KEY_USER_EMAIL               = "k_user_email"
    static let KEY_USER_FNAME               = "k_user_fname"
    static let KEY_USER_LNAME               = "k_user_lname"
    static let KEY_USER_USERNAME            = "k_username"
    static let KEY_EVENTID                  = "k_eventid"
    static let KEY_EVENTNAME                = "k_eventname"
    static let KEY_EVENTPWD                 = "k_eventpwd"
    static let KEY_USERPWD                  = "k_userpwd"
    static let KEY_EVENTLIST                = "k_eventlist"
    static let KEY_LOGINTYPE                = "k_logintype"
    static let KEY_ISAPPROVED               = "k_is_approved"
    
    
    
    // Result code
    static let RESULT_CODE                  = "result_code"

    static let CODE_ERROR                   = -100
    static let CODE_SUCCESS                 = 200
    static let CODE_EXIST_EVENT             = 201
    static let CODE_INVALID_LOGIN           = 202
    static let CODE_UPLOAD_FAIL             = 203

    
}
