//
//  CommonUtils.swift
//  Kaali
//
//  Created by Yin on 25/03/2018.
//  Copyright © 2018 Yin. All rights reserved.
//

import Foundation

class CommonUtils {
    
    static func isValidEmail(_ email: String) -> Bool {
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }
}
var event_photos = [PhotoModel]()

var currentUser : UserModel? {
    didSet {
        if let user = currentUser {
            UserDefaults.standard.set(user.email, forKey: Const.KEY_USER_EMAIL)
            UserDefaults.standard.set(user.user_name, forKey: Const.KEY_USER_USERNAME)
            UserDefaults.standard.set(user.first_name, forKey: Const.KEY_USER_FNAME)
            UserDefaults.standard.set(user.last_name, forKey: Const.KEY_USER_LNAME)
            UserDefaults.standard.set(user.user_id, forKey: Const.KEY_USERID)
            UserDefaults.standard.set(user.password, forKey: Const.KEY_USERPWD)
            UserDefaults.standard.set(user.password, forKey: Const.KEY_LOGINTYPE)
        }
    }
}

func getCurrentUser() -> UserModel {
    let user = UserModel()
    user.email = UserDefaults.standard.string(forKey: Const.KEY_USER_EMAIL)!
    user.user_id = UserDefaults.standard.integer(forKey: Const.KEY_USERID)
    user.first_name = UserDefaults.standard.string(forKey: Const.KEY_USER_FNAME)!
    user.last_name = UserDefaults.standard.string(forKey: Const.KEY_USER_LNAME)!
    user.password = UserDefaults.standard.string(forKey: Const.KEY_USERPWD) ?? ""
    user.user_name = UserDefaults.standard.string(forKey: Const.KEY_USER_USERNAME) ?? ""
    user.login_type = UserDefaults.standard.integer(forKey: Const.KEY_LOGINTYPE)
    return user    
}

var currentEvent : EventModel? {
    didSet {
        if let event = currentEvent {
            UserDefaults.standard.set(event.event_name, forKey: Const.KEY_EVENTNAME)
            UserDefaults.standard.set(event.event_id, forKey: Const.KEY_EVENTID)
            UserDefaults.standard.set(event.event_password, forKey: Const.KEY_EVENTPWD)
            UserDefaults.standard.set(event.event_userid, forKey: Const.KEY_USERID)
            UserDefaults.standard.set(event.is_approved, forKey: Const.KEY_ISAPPROVED)
        }
    }
}


func resizeImage(srcImage: UIImage) -> UIImage {
    
    if (srcImage.size.width >= srcImage.size.height) {
        
        return srcImage.resizedImage(byMagick: "512")
    } else {
        
        return srcImage.resizedImage(byMagick: "x512")
    }
}


// save image to a file (Documents/SmarterApp/temp.png)
func saveToFile(image: UIImage!, filePath: String!, fileName: String) -> String! {
    
    let outputFileName = fileName
    
    let outputImage = resizeImage(srcImage: image)
    
    let fileManager = FileManager.default
    let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
    var documentDirectory: NSString = paths[0] as NSString
    
    // current document directory
    fileManager.changeCurrentDirectoryPath(documentDirectory as String)
    
    do {
        try fileManager.createDirectory(atPath: filePath, withIntermediateDirectories: true, attributes: nil)
    } catch let error as NSError {
        print(error.localizedDescription)
    }
    
    documentDirectory = documentDirectory.appendingPathComponent(filePath) as NSString!
    let savedFilePath = documentDirectory.appendingPathComponent(outputFileName)
    
    // if the file exists already, delete and write, else if create filePath
    if (fileManager.fileExists(atPath: savedFilePath)) {
        do {
            try fileManager.removeItem(atPath: savedFilePath)
        }
        catch let error as NSError {
            print("Ooops! Something went wrong: \(error)")
        }
    } else {
        fileManager.createFile(atPath: savedFilePath, contents: nil, attributes: nil)
    }
    
    if let data = UIImagePNGRepresentation(outputImage) {
        
        do {
            try data.write(to:URL(fileURLWithPath:savedFilePath), options:.atomic)
        } catch {
            print(error)
        }
    }
    return savedFilePath
}
