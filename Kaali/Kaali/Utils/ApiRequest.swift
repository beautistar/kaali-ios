//
//  ApiRequest.swift
//  Kaali
//
//  Created by Yin on 25/03/2018.
//  Copyright © 2018 Yin. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON


class ApiRequest {
    
    static let BASE_URL = "http://34.217.253.29/"
    static let SERVER_URL = BASE_URL + "app/api/"
    
    static let REQ_FBLOGIN                  = SERVER_URL + "fb_login"
    static let REQ_CREATE_EVENT             = SERVER_URL + "create_event"
    static let REQ_LOGIN_EVENT              = SERVER_URL + "login_event"
    static let REQ_UPLOAD_PHOTO             = SERVER_URL + "upload_photo"
    static let REQ_GET_PHOTOS               = SERVER_URL + "get_photos"
    static let REQ_SEND_ORDER               = SERVER_URL + "send_order"
    static let REQ_STRIPE_PAYMENT           = SERVER_URL + "stripe_payment"
    static let REQ_GET_PRICES               = SERVER_URL + "get_prices"
    static let REQ_REGISTERUSER             = SERVER_URL + "register_user"
    static let REQ_LOGINUSER                = SERVER_URL + "login_user"
    static let REQ_APPROVE                  = SERVER_URL + "approve_photo"
    static let REQ_GETEVENT                 = SERVER_URL + "get_event"
    
    // API functions
    static func fb_login(user: UserModel, completion: @escaping (String) -> () ) {
        
        let params = [Const.PARAM_USERNAME: user.user_name,
                      Const.PARAM_FIRSTNAME: user.first_name,
                      Const.PARAM_LASTNAME: user.last_name,
                      Const.PARAM_EMAIL: user.email] as [String : Any]
        
        Alamofire.request(REQ_FBLOGIN, method: .post, parameters: params).responseJSON { response in
            
            print(response)
            
            if response.result.isFailure{
                completion(Const.ERROR_CONNECT)
            }
            else
            {
                let json = JSON(response.result.value!)
                
                let resCode = json[Const.RESULT_CODE].intValue
                
                if resCode == Const.CODE_SUCCESS {
                    
                    user.user_id = json[Const.PARAM_USERID].intValue
                    user.login_type = 0
                    currentUser = user
                    
                    completion(Const.LOGIN_SUCCESS)
                }
            }
        }
    }
    
    static func create_event(event: EventModel, completion: @escaping (String) -> () ) {
        
        let params = [Const.PARAM_EVENTNAME: event.event_name,
                      Const.PARAM_EVENTID: event.event_id,
                      Const.PARAM_EVENT_PASSWORD: event.event_password,
                      Const.PARAM_USERID: currentUser!.user_id,
                      Const.PARAM_ISAPPOVED: event.is_approved] as [String : Any]
        
        Alamofire.request(REQ_CREATE_EVENT, method: .post, parameters: params).responseJSON { response in
            
            print(response)
            
            if response.result.isFailure{
                completion(Const.ERROR_CONNECT)
            }
            else
            {
                let json = JSON(response.result.value!)
                
                let resCode = json[Const.RESULT_CODE].intValue
                
                if resCode == Const.CODE_SUCCESS {
                    currentEvent = event
                    completion(Const.SUCCESS)
                } else {
                    completion(Const.EXIST_EVENT)
                }
            }
        }
    }
    
    static func login_event(event_id: String, event_password: String, completion: @escaping (String) -> () ) {
        
        let params = [Const.PARAM_USERID: currentUser!.user_id,
                      Const.PARAM_EVENTID: event_id,
                      Const.PARAM_EVENT_PASSWORD: event_password] as [String : Any]
        
        Alamofire.request(REQ_LOGIN_EVENT, method: .post, parameters: params).responseJSON { response in
            
            print(response)
            
            if response.result.isFailure{
                completion(Const.ERROR_CONNECT)
            }
            else
            {
                let json = JSON(response.result.value!)
                
                let resCode = json[Const.RESULT_CODE].intValue
                
                if resCode == Const.CODE_SUCCESS {
                    currentEvent = ParseHelper.parse_event(json)
                    completion(Const.LOGIN_SUCCESS)
                } else {
                    completion(Const.LOGIN_FAILED)
                }
            }
        }
    }
    
    static func upload_photo(_ imgURL: String, completion: @escaping (String) -> ()) {
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                multipartFormData.append(URL(fileURLWithPath: imgURL), withName: "file")
                multipartFormData.append(String(currentUser!.user_id).data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: Const.PARAM_USERID)
                multipartFormData.append(currentEvent!.event_id.data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: Const.PARAM_EVENTID)
                multipartFormData.append(String(currentEvent!.is_approved).data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: Const.PARAM_ISAPPOVED)
        },
            to: REQ_UPLOAD_PHOTO,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        
                        switch response.result {
                            
                        case .success(_):
                            let json = JSON(response.result.value!)
                            let resCode = json[Const.RESULT_CODE].intValue
                            if (resCode == Const.CODE_SUCCESS) {
                                completion(Const.UPLOAD_SUCCESS)
                            }
                            else {
                                completion(Const.UPLOAD_FAIL)
                            }
                        case .failure(_):
                            
                            completion(Const.ERROR_CONNECT)
                        }
                    }
                case .failure(_):
                    completion(Const.ERROR_CONNECT)
                }
        })
    }
    
    static func get_photos(completion: @escaping (String, [PhotoModel]) -> () ) {
        
        let params = [Const.PARAM_EVENTID: currentEvent!.event_id] as [String : Any]
        
        Alamofire.request(REQ_GET_PHOTOS, method: .post, parameters: params).responseJSON { response in
            
            print(response)
            
            if response.result.isFailure{
                completion(Const.ERROR_CONNECT, [])
            }
            else
            {
                let json = JSON(response.result.value!)
                
                let resCode = json[Const.RESULT_CODE].intValue
                
                if resCode == Const.CODE_SUCCESS {
                    let phptos_array = json[Const.PARAM_EVENT_PHOTOS].arrayValue
                    var photos = [PhotoModel]()
                    for photoObj in phptos_array {
                        photos.append(ParseHelper.parse_photo(photoObj))
                    }
                    
                    completion(Const.SUCCESS, photos)
                } 
            }
        }
    }
    
    static func stripe_payment(_ token: String, amount: Float, completion: @escaping (Int, String) -> ()) {
        
        let params = [Const.PARAM_EMAIL: currentUser!.email,
                      Const.PARAM_STRIPETOKEN: token,
                      Const.PARAM_AMOUNT: amount] as [String: Any]
        
        Alamofire.request(REQ_STRIPE_PAYMENT, method: .post, parameters: params).responseJSON { response in
            
            print(response)
            
            if response.result.isFailure {
                completion(Const.CODE_ERROR, Const.ERROR_CONNECT)
            } else {
                
                let json = JSON(response.result.value!)
                let resCode = json[Const.RESULT_CODE].intValue
                
                if resCode == Const.CODE_SUCCESS {
                    completion(resCode, json[Const.PARAM_ID].stringValue)
                } else {
                    completion(resCode, json[Const.PARAM_MESSAGE].stringValue)
                }
            }
        }
    }
    
    static func send_order(_ order_list: [PhotoModel], transaction_id: String, completion: @escaping (String) -> ()) {
        
        
        var array: [Any] = []
        for order in order_list {
            let order_obj: [String: Any] = [Const.PARAM_PHOTOID: order.photo_id,
                             Const.PARAM_USERID: order.user_id,
                             Const.PARAM_EVENTID: order.event_id,
                             Const.PARAM_EMAIL: currentUser!.email,
                             Const.PARAM_TRANSACTIONID: transaction_id,
                             Const.PARAM_COMMENT: order.comment]

            array.append(order_obj)
        }
        
        let orders: [String: Any] = [Const.PARAM_ORDERLIST: array]
        
        Alamofire.request(REQ_SEND_ORDER, method: .post, parameters: orders, encoding: JSONEncoding.default).responseJSON { response in

            print(response)

            if response.result.isFailure{
                completion(Const.ERROR_CONNECT)
            }
            else
            {
                let json = JSON(response.result.value!)

                let resCode = json[Const.RESULT_CODE].intValue

                if resCode == Const.CODE_SUCCESS {
                    completion(Const.SUCCESS)
                }
            }
        }
    }
    
    
    static func get_prices(completion: @escaping (String, Float, Float) -> () ) {
        
        Alamofire.request(REQ_GET_PRICES, method: .post).responseJSON { response in
            
            print(response)
            
            if response.result.isFailure{
                completion(Const.ERROR_CONNECT, 0, 0)
            }
            else
            {
                let json = JSON(response.result.value!)
                
                let resCode = json[Const.RESULT_CODE].intValue
                
                if resCode == Const.CODE_SUCCESS {
                    
                    let price = json[Const.PARAM_PRICE].floatValue
                    let service_fee = json[Const.PARAM_SERVICEFEE].floatValue
                    
                    completion(Const.SUCCESS, price, service_fee)
                }
            }
        }
    }
    
    static func register_user(user: UserModel, completion: @escaping (String) -> () ) {
        
        let params = [Const.PARAM_USERNAME: user.user_name,
                      Const.PARAM_PASSWORD: user.password] as [String : Any]
        
        Alamofire.request(REQ_REGISTERUSER, method: .post, parameters: params).responseJSON { response in
            
            print(response)
            
            if response.result.isFailure{
                completion(Const.ERROR_CONNECT)
            }
            else
            {
                let json = JSON(response.result.value!)
                
                let resCode = json[Const.RESULT_CODE].intValue
                
                if resCode == Const.CODE_SUCCESS {
                    
                    user.user_id = json[Const.PARAM_USERID].intValue
                    user.login_type = 1
                    currentUser = user
                    completion(Const.REGISTER_SUCCESS)
                } else {
                    completion(Const.EXIST_EVENT)
                }
            }
        }
    }
    
    static func login_user(user_name: String, password: String, completion: @escaping (String) -> () ) {
        
        let params = [Const.PARAM_USERNAME: user_name,
                      Const.PARAM_PASSWORD: password] as [String : Any]
        
        Alamofire.request(REQ_LOGINUSER, method: .post, parameters: params).responseJSON { response in
            
            print(response)
            
            if response.result.isFailure{
                completion(Const.ERROR_CONNECT)
            }
            else
            {
                let json = JSON(response.result.value!)
                
                let resCode = json[Const.RESULT_CODE].intValue
                
                if resCode == Const.CODE_SUCCESS {
                    let user = UserModel()
                    user.user_id = json[Const.PARAM_USERID].intValue
                    user.user_name = user_name
                    user.password = password
                    user.login_type = 1
                    currentUser = user
                    
                    completion(Const.LOGIN_SUCCESS)
                } else {
                    completion(Const.LOGIN_FAILED)
                }
            }
        }
    }
    
    static func approve_photo(photo_id: Int, completion: @escaping (String) -> () ) {
        
        let params = [Const.PARAM_PHOTOID: photo_id] as [String : Any]
        Alamofire.request(REQ_APPROVE, method: .post, parameters: params).responseJSON { (response) in
            
            print(response)
            
            if response.result.isSuccess {
                completion(Const.SUCCESS)
            } else {
                completion(Const.ERROR_CONNECT)
            }
        }
    }
    
    static func get_event(completion: @escaping (Int, [EventModel]) -> ()) {
        
        let params = [Const.PARAM_USERID: currentUser!.user_id]
        
        Alamofire.request(REQ_GETEVENT, method: .post, parameters: params).responseJSON { (response) in
            print("------------------get saved event list-----------------------------------------")
            print(response)
            
            if response.result.isSuccess {
                let json = JSON(response.result.value!)
                let resCode = json[Const.RESULT_CODE].intValue
                if resCode == Const.CODE_SUCCESS {
                    var eventList = [EventModel]()
                    let eventArray = json[Const.PARAM_EVENTLIST].arrayValue
                    for event in eventArray {
                        let event = ParseHelper.parse_event(event)
                        eventList.append(event)
                    }
                    completion(Const.CODE_SUCCESS, eventList)
                }
            } else {
                completion(Const.CODE_ERROR, [])
            }
        }
    }

}
