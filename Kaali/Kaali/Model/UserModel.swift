//
//  UserModel.swift
//  Kaali
//
//  Created by Yin on 25/03/2018.
//  Copyright © 2018 Yin. All rights reserved.
//

import Foundation

class UserModel {
    
    var user_id: Int = 0
    var user_name = ""
    var email = ""
    var first_name = ""
    var last_name = ""
    var password = ""
    var login_type = 0 // 0: facebook, 1:username
    
}
