//
//  EventModel.swift
//  Kaali
//
//  Created by Yin on 25/03/2018.
//  Copyright © 2018 Yin. All rights reserved.
//

import Foundation

class EventModel: NSObject, NSCoding {
    
    var event_name = ""
    var event_id = ""
    var event_password = ""
    var event_userid = 0
    var is_approved = 1
    
    init(event_name: String, event_id: String, event_password: String, event_userid: Int, is_approved: Int) {
        
        
        self.event_name = event_name
        self.event_id = event_id
        self.event_password = event_password
        self.event_userid = event_userid
        self.is_approved = is_approved
    }
    
    func encode(with aCoder: NSCoder) {

        aCoder.encode(event_name, forKey: "e_event_name")
        aCoder.encode(event_id, forKey: "e_event_id")
        aCoder.encode(event_password, forKey: "e_event_password")
        aCoder.encode(event_userid, forKey: "e_event_userid")
        aCoder.encode(is_approved, forKey: "e_approved")
    }
    
    required convenience init(coder aDecoder: NSCoder) {        
        
        let event_name = aDecoder.decodeObject(forKey: "e_event_name") as! String
        let event_id = aDecoder.decodeObject(forKey: "e_event_id") as! String
        let event_password = aDecoder.decodeObject(forKey: "e_event_password") as! String
        let event_userid = aDecoder.decodeInteger(forKey: "e_event_userid")
        let is_approved = aDecoder.decodeInteger(forKey: "e_approved")
        self.init(event_name: event_name, event_id: event_id, event_password: event_password, event_userid: event_userid, is_approved: is_approved)
    }
}
