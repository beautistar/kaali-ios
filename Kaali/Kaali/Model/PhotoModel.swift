//
//  PhotoModel.swift
//  Kaali
//
//  Created by Yin on 09/03/2018.
//  Copyright © 2018 Yin. All rights reserved.
//

import Foundation

class PhotoModel {
    
    var photo_id: Int = 0
    var user_id: Int = 0
    var event_id: String = ""
    var comment: String = ""
    var photo_url: String = ""
    var is_approved: Int = 1

    var isChecked: Bool = false
    
}
