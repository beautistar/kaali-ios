//
//  PageViewController.swift
//  Kaali
//
//  Created by Yin on 06/03/2018.
//  Copyright © 2018 Yin. All rights reserved.
//

import UIKit

class PageViewController: BasePageViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate, UIScrollViewDelegate {
    
    var libraryEnabled: Bool = true
    var croppingEnabled: Bool = false
    var allowResizing: Bool = true
    var allowMoving: Bool = true
    var minimumSize: CGSize = CGSize(width: 100, height: 100)
    
    
    
    var croppingParameters: CroppingParameters {
        return CroppingParameters(isEnabled: croppingEnabled, allowResizing: allowResizing, allowMoving: allowMoving, minimumSize: minimumSize)
    }
    
    /*
    let libraryViewController = CameraViewController.imagePickerViewController(croppingParameters: croppingParameters) { [weak self] image, asset in
        self?.imageView.image = image
        self?.dismiss(animated: true, completion: nil)
    }
     */
    
    lazy var subViewControllers: [UIViewController] = {
        
        return [self.storyboard?.instantiateViewController(withIdentifier: "EventViewController") as! EventViewController,
                
                
                CameraViewController(croppingParameters: croppingParameters, allowsLibraryAccess: libraryEnabled) { [weak self] image, asset in

                    let image_url = saveToFile(image: image, filePath: Const.SAVE_ROOT_PATH, fileName: "photo.png")

                    self?.showLoadingView()

                    ApiRequest.upload_photo(image_url!, completion: { (message) in

                        self?.hideLoadingView()

                        if message == Const.UPLOAD_SUCCESS {
                            self?.showToast(message)

                            ApiRequest.get_photos(completion: { (meesage, photos) in

                                event_photos.removeAll()
                                event_photos = photos
                            })

                        } else {
                            self?.showAlertDialog(title: Const.ERROR, message: message, positive: Const.OK, negative: nil)
                        }
                    })
                },
 
                self.storyboard?.instantiateViewController(withIdentifier: "ViewPhotosViewController") as! ViewPhotosViewController
        ]
    }()

    var currentIndex: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.delegate = self
        self.dataSource = self
        // Do any additional setup after loading the view.
        
        //set middle viewcontroller to appear at first time.
        currentIndex = 1
        setViewControllers([subViewControllers[1]], direction: .forward, animated: true, completion: nil)
        
        for view in self.view.subviews {
            if let scrollView = view as? UIScrollView {
                scrollView.delegate = self
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        UIApplication.shared.isStatusBarHidden = true
        //self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //self.navigationController?.isNavigationBarHidden = true
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if currentIndex == 0 && scrollView.contentOffset.x < scrollView.bounds.size.width {
            scrollView.contentOffset = CGPoint(x: scrollView.bounds.size.width, y: 0)
        } else if currentIndex == subViewControllers.count - 1 && scrollView.contentOffset.x > scrollView.bounds.size.width {
            scrollView.contentOffset = CGPoint(x: scrollView.bounds.size.width, y: 0)
        }
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        if currentIndex == 0 && scrollView.contentOffset.x < scrollView.bounds.size.width {
            scrollView.contentOffset = CGPoint(x: scrollView.bounds.size.width, y: 0)
        } else if currentIndex == subViewControllers.count - 1 && scrollView.contentOffset.x > scrollView.bounds.size.width {
            scrollView.contentOffset = CGPoint(x: scrollView.bounds.size.width, y: 0)
        }
    }
    
    // MARK - UIPageViewControllerDataSource
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if completed {
            guard let viewController = pageViewController.viewControllers?.first,
                let index = subViewControllers.index(of: viewController) else {
                    fatalError("Can't prevent bounce if there's not an index")
            }
            currentIndex = index
        }
    }
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return subViewControllers.count
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        currentIndex = subViewControllers.index(of: viewController) ?? 0
        if currentIndex <= 0 {
            return nil
        }
        return subViewControllers[currentIndex-1]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        currentIndex = subViewControllers.index(of: viewController) ?? 0
        
        if (currentIndex >= subViewControllers.count-1) {
            return nil
        }
        
        return subViewControllers[currentIndex+1]
    }

}


