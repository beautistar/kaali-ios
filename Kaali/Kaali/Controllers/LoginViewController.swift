//
//  LoginViewController.swift
//  Kaali
//
//  Created by Yin on 08/03/2018.
//  Copyright © 2018 Yin. All rights reserved.
//

import UIKit
import Presentr

class LoginViewController: BaseViewController {
    
    @IBOutlet weak var tfEventID: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var imvCheck: UIImageView!
    
    let presenter: Presentr = {
        let presenter = Presentr(presentationType: .alert)
        presenter.transitionType = TransitionType.coverHorizontalFromRight
        presenter.dismissOnSwipe = true
        return presenter
    }()

    var isChecked: Bool = false

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        initView()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initView() {
        
        UIApplication.shared.statusBarView?.backgroundColor = UIColor.clear
        imvCheck.image = nil
        
        let event_id = UserDefaults.standard.string(forKey: Const.KEY_EVENTID)
        if event_id != nil {
            tfEventID.text = event_id
            tfPassword.text = UserDefaults.standard.string(forKey: Const.KEY_EVENTPWD)
        }
    }
    
    
    @IBAction func onCheck(_ sender: Any) {
        
        isChecked = !isChecked
        
        if isChecked {
            imvCheck.image = #imageLiteral(resourceName: "checked")
        } else {
            imvCheck.image = nil
        }
    }
    
    func isValid() -> Bool {
        
        if tfEventID.text?.count == 0 {
            self.showAlertDialog(title: Const.ERROR, message: Const.INPUT_EVENTID, positive: Const.OK, negative: nil)
            return false
        }
        
        if tfPassword.text?.count == 0 {
            self.showAlertDialog(title: Const.ERROR, message: Const.INPUT_PASSWORD, positive: Const.OK, negative: nil)
            return false
        }
        
        if !isChecked {
            self.showAlertDialog(title: Const.ERROR, message: Const.AGREE_TERM, positive: Const.OK, negative: nil)
            return false
        }
        
        return true
    }
    
    @IBAction func onLogin(_ sender: Any) {
        
        if isValid() {
            self.doLogin()
        }
    }
    
    func doLogin() {
        
        self.showLoadingView()
        
        ApiRequest.login_event(event_id: tfEventID.text!, event_password: tfPassword.text!) { (message) in
            
            self.hideLoadingView()
            
            if message == Const.LOGIN_SUCCESS {

                self.gotoMain()
                
            } else {
                self.showAlertDialog(title: Const.ERROR, message: message, positive: Const.OK, negative: nil)
            }
        }
    }
    
    @IBAction func onCreateEvent(_ sender: Any) {
    
        gotoCreateEvent()
    }
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    

    
    func gotoCreateEvent() {
        
        let createEventVC = self.storyboard?.instantiateViewController(withIdentifier: "CreateEventViewController") as! CreateEventViewController
        
        presenter.presentationType = .custom(width: ModalSize.fluid(percentage: 0.9), height: ModalSize.fluid(percentage: 0.75), center: ModalCenterPosition.center)
        
        presenter.transitionType = nil
        presenter.dismissTransitionType = nil
        presenter.dismissAnimated = true
        customPresentViewController(presenter, viewController: createEventVC, animated: true, completion: nil)
    }
    
    func gotoMain() {
        
        UIApplication.shared.keyWindow?.rootViewController = self.storyboard?.instantiateViewController(withIdentifier: "PageViewController") as! PageViewController        
    }
}
