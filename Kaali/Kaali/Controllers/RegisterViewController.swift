//
//  RegisterViewController.swift
//  Kaali
//
//  Created by Yin on 2018/10/4.
//  Copyright © 2018 Yin. All rights reserved.
//

import UIKit

class RegisterViewController: BaseViewController {

    @IBOutlet weak var tfUserName: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var tfConfirmPassword: UITextField!
    
    var user = UserModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func isValid() -> Bool {
        
        if tfUserName.text?.count == 0 {
            self.showAlertDialog(title: Const.ERROR, message: Const.INPUT_USERNAME, positive: Const.OK, negative: nil)
            return false
        }
        
        if tfPassword.text?.count == 0 {
            self.showAlertDialog(title: Const.ERROR, message: Const.INPUT_PASSWORD, positive: Const.OK, negative: nil)
            return false
        }
        
        if tfPassword.text != tfConfirmPassword.text {
            self.showAlertDialog(title: Const.ERROR, message: Const.MATCH_PASSWORD, positive: Const.OK, negative: nil)
            return false
        }
        
        return true
    }
    
    //MARK: - Button Actions
    @IBAction func regsiterAction(_ sender: Any) {
        if isValid() {
            doRegister()
        }
    }
    
    func doRegister() {
        
        user.user_name = tfUserName.text!
        user.password = tfPassword.text!
        self.showLoadingView()
        
        ApiRequest.register_user(user: user) { (message) in
            self.hideLoadingView()
            if message == Const.REGISTER_SUCCESS {
                self.gotoEventList()
            } else {
                self.showAlertDialog(title: Const.APP_NAME, message: message, positive: Const.OK, negative: nil)
            }
        }
        
    }
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func gotoEventList() {
        let eventListVC = self.storyboard?.instantiateViewController(withIdentifier: "EventListViewController") as! EventListViewController
        self.navigationController?.pushViewController(eventListVC, animated: true)
    }

}
