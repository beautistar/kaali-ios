//
//  CreateEventViewController.swift
//  Kaali
//
//  Created by Yin on 08/03/2018.
//  Copyright © 2018 Yin. All rights reserved.
//

import UIKit

class CreateEventViewController: BaseViewController, UITextFieldDelegate {
    
    var isChecked: Bool = false

    @IBOutlet weak var tfEventName: UITextField!
    @IBOutlet weak var tfEvnetID: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var imvCheck: UIImageView!
    @IBOutlet weak var imvImmediApprove: UIImageView!
    @IBOutlet weak var imvLaterApprove: UIImageView!

    var isApproved = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        UIApplication.shared.statusBarView?.backgroundColor = UIColor.clear
        
        imvCheck.image = nil
        
        imvImmediApprove.layer.borderColor = Const.mainColor.cgColor
        imvLaterApprove.layer.borderColor = Const.mainColor.cgColor
        
        imvImmediApprove.image = UIImage(named: "confirmButton")

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onCheck(_ sender: Any) {
        
        isChecked = !isChecked
        
        if isChecked {
            imvCheck.image = #imageLiteral(resourceName: "checked")
        } else {
            imvCheck.image = nil
        }
    }
    
    @IBAction func onOptionCheck(_ sender: Any) {
        
        let button = sender as! UIButton
        let tag = button.tag
        
        if tag == 0 {
            isApproved = 1
            imvImmediApprove.image = UIImage(named: "confirmButton")
            imvLaterApprove.image = nil
        } else {
            isApproved = 0
            imvImmediApprove.image = nil
            imvLaterApprove.image = UIImage(named: "confirmButton")
        }
    }
    
    
    func isValid() -> Bool {
        
        if tfEventName.text?.count == 0 {
            self.showAlertDialog(title: Const.ERROR, message: Const.INPUT_EVENTNAME, positive: Const.OK, negative: nil)
            return false
        }
        
        if tfEvnetID.text?.count == 0 {
            self.showAlertDialog(title: Const.ERROR, message: Const.INPUT_EVENTID, positive: Const.OK, negative: nil)
            return false
        }
        
        if tfPassword.text?.count == 0 {
            self.showAlertDialog(title: Const.ERROR, message: Const.INPUT_PASSWORD, positive: Const.OK, negative: nil)
            return false
        }
        
        if !isChecked {
            self.showAlertDialog(title: Const.ERROR, message: Const.AGREE_TERM, positive: Const.OK, negative: nil)
            return false
        }
        
        return true
    }
    
    @IBAction func onGo(_ sender: Any) {
        
        if isValid() {
            self.doCreateEvent()
        }
    }
    
    func doCreateEvent() {
        
        self.showLoadingView()
        
        let event_id = tfEvnetID.text!
        let event_name = tfEventName.text!
        let event_password = tfPassword.text!
        let event_userid = currentUser!.user_id
        
        let event = EventModel.init(event_name: event_name, event_id: event_id, event_password: event_password, event_userid: event_userid, is_approved: isApproved)
        
        ApiRequest.create_event(event: event) { (message) in
            
            self.hideLoadingView()
            
            if message == Const.SUCCESS {
                self.gotoMain()                
            } else {
                self.showAlertDialog(title: Const.ERROR, message: message, positive: Const.OK, negative: nil)
            }
        }
    }
    
    func gotoMain() {
        UIApplication.shared.keyWindow?.rootViewController = self.storyboard?.instantiateViewController(withIdentifier: "PageViewController") as! PageViewController
    }

    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == tfEventName {
            self.tfEvnetID.becomeFirstResponder()
        } else if textField == tfEvnetID {
            tfPassword.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        
        return false
    }

}
