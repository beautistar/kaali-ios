//
//  PaymentViewController.swift
//  Kaali
//
//  Created by Yin on 10/03/2018.
//  Copyright © 2018 Yin. All rights reserved.
//

import UIKit
import Stripe


class PaymentViewController: BaseViewController,PayPalPaymentDelegate {
    
    
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var chooseView: UIView!
    @IBOutlet weak var cardView: STPPaymentCardTextField!
    @IBOutlet weak var imvCardCheck: UIImageView!
    @IBOutlet weak var imvPayPalCheck: UIImageView!
    @IBOutlet weak var lblTotalCount: UILabel!
    @IBOutlet weak var lblOnePrice: UILabel!
    @IBOutlet weak var lblServiceFee: UILabel!
    @IBOutlet weak var lblTotalPrice: UILabel!
    @IBOutlet weak var btnComplete: UIButton!
    
    
    var isCardChecked: Bool = true
    
    var order_photos = [PhotoModel]()
    
    var one_price: Float = 0.0
    var service_fee: Float = 0.0
    var total_price: Float = 0.0
    
    fileprivate var paypalEnvironment: String = PayPalEnvironmentProduction /*PayPalEnvironmentSandbox*/  {
        willSet (newEnviroment) {
            if newEnviroment != paypalEnvironment {
                PayPalMobile.preconnect(withEnvironment: newEnviroment)
            }
        }
    }
    //    fileprivate var paypalConfig = PayPalConfiguration()
    var paypalResultText = ""
    var paypalConfig = PayPalConfiguration()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        initView()
        
        configurationPaypal()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initView() {
        
        UIApplication.shared.statusBarView?.backgroundColor = Const.mainColor
        
        infoView.layer.borderColor = UIColor.lightGray.cgColor
        chooseView.layer.borderColor = UIColor.lightGray.cgColor
        
        imvCardCheck.image = #imageLiteral(resourceName: "confirmButton")
        imvPayPalCheck.image = nil
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        imvCardCheck.layer.borderColor = Const.mainColor.cgColor
        imvPayPalCheck.layer.borderColor = Const.mainColor.cgColor
        
        PayPalMobile.preconnect(withEnvironment: paypalEnvironment)
        
        getPrices()
    }
    
    func getPrices() {
        
        self.showLoadingViewWithTitle(title: "Calculating...")
        
        ApiRequest.get_prices { (message, price, service_fee) in
            
            self.hideLoadingView()
            
            if message == Const.SUCCESS {
                self.one_price = price
                self.service_fee = service_fee
                self.calculate_prices()
            }
        }
    }
    
    func calculate_prices() {
        
        self.total_price = self.one_price * Float(order_photos.count) + self.service_fee
        
        // set infomation
        lblTotalCount.text = "\(order_photos.count)"
        lblOnePrice.text = "\(one_price)"
        lblServiceFee.text = "\(service_fee)"
        lblTotalPrice.text = "\(self.total_price)"
    }
    
    fileprivate func configurationPaypal() {
        
        paypalConfig.acceptCreditCards = false
        paypalConfig.merchantName = String(format: "%@ %@", currentUser!.first_name, currentUser!.last_name)
        paypalConfig.merchantPrivacyPolicyURL = URL(string: "https://www.paypal.com/webapps/mpp/ua/privacy-full")
        paypalConfig.merchantUserAgreementURL = URL(string: "https://www.paypal.com/webapps/mpp/ua/useragreement-full")
        paypalConfig.languageOrLocale = Locale.preferredLanguages[0]
        paypalConfig.payPalShippingAddressOption = .payPal
    }
    
    @IBAction func onSwitch(_ sender: Any) {
        
        let btn = sender as! UIButton
        isCardChecked = !isCardChecked
        if btn.tag == 0 {
            imvCardCheck.image = #imageLiteral(resourceName: "confirmButton")
            imvPayPalCheck.image = nil
            cardView.isHidden = false
        } else {
            imvCardCheck.image = nil
            imvPayPalCheck.image = #imageLiteral(resourceName: "confirmButton")
            cardView.isHidden = true
        }
    }
    @IBAction func onCancel(_ sender: Any) {
        
        closeView()
    }
    
    @objc func closeView() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onComplete(_ sender: Any) {
        
        if isCardChecked {
            payWithStripe()
        } else {
            payWithPayPal()
        }
    }
    
    func payWithStripe() {
        
        self.view.endEditing(true)
        
        self.showLoadingView()
        
        let cardParams = STPCardParams()
        
        cardParams.number = cardView.cardNumber
        cardParams.expMonth = cardView.expirationMonth
        cardParams.expYear = cardView.expirationYear
        cardParams.cvc = cardView.cvc
        
        
        STPAPIClient.shared().createToken(withCard: cardParams) { (token: STPToken?, error: Error?) in
            if token != nil {
                
                ApiRequest.stripe_payment(token!.tokenId, amount: self.total_price*100, completion: { (resCode, message) in
                    
                    self.hideLoadingView()
                    if resCode == Const.CODE_SUCCESS {
                        
                        self.send_order(message)
                        
                    } else {
                        self.showAlertDialog(title: Const.ERROR, message: message, positive: Const.OK, negative: nil)
                    }
                })
                
            } else {
                self.showAlertDialog(title: Const.ERROR, message: error?.localizedDescription, positive: Const.OK, negative: nil)
                self.hideLoadingView()
            }
        }
    }
    
    func payWithPayPal() {
        
        let donateDescription = "Amount ";
        let paypalPayment = PayPalPayment(amount: NSDecimalNumber(value: total_price), currencyCode: "USD", shortDescription: donateDescription, intent: .order)
        
        if paypalPayment.processable {
            let paymentVC = PayPalPaymentViewController(payment: paypalPayment, configuration: paypalConfig, delegate: self)!
            self.present(paymentVC, animated: true, completion: nil)
        } else {
            self.showAlertDialog(title: Const.ERROR, message: Const.ERROR_PAYPALPAY, positive: Const.OK, negative: nil)
        }        
    }
    
    
    func send_order(_ transactionId: String) {
        
        self.showLoadingView()
        
        ApiRequest.send_order(order_photos, transaction_id: transactionId) { (message) in
            
            self.hideLoadingView()
            
            if message == Const.SUCCESS {
                //self.showToast(message)
                
                let confirmAlert = UIAlertController(title: Const.SUCCESS, message: "Your Transaction ID is " + transactionId + " \nYour edited photos will be sent to " + currentUser!.email + "\n\nKeep a screenshot of this for you!", preferredStyle: .alert)
                
                confirmAlert.addAction(UIAlertAction(title: Const.OK, style: .default, handler: { (_) in
                    
                    isPaid = true
                    
                    self.perform(#selector(self.closeView), with: nil, afterDelay: 0.2)
                   
                }))
                
               self.present(confirmAlert, animated: true, completion: nil)
                
            } else {
                self.showAlertDialog(title: Const.ERROR, message: message, positive: Const.OK, negative: nil)
            }
        }
    }
    
    func payPalPaymentDidCancel(_ paymentViewController: PayPalPaymentViewController) {
        
        paymentViewController.dismiss(animated: true, completion: nil)
        self.showAlertDialog(title: "Canceled", message: "You have canceled payment transaction.", positive: Const.OK, negative: nil)
        
    }
    
    func payPalPaymentViewController(_ paymentViewController: PayPalPaymentViewController, didComplete completedPayment: PayPalPayment) {
        print(completedPayment)
        
        
        paymentViewController.dismiss(animated: true, completion: nil)
        // TODO: Change transaction id
        let transactionId = (completedPayment.confirmation["response"] as! [AnyHashable: Any])["id"] as! String
        
        print(transactionId)
        
        send_order(transactionId)
        
    }
    
    func payPalPaymentViewController(_ paymentViewController: PayPalPaymentViewController, willComplete completedPayment: PayPalPayment, completionBlock: @escaping PayPalPaymentDelegateCompletionBlock) {
        paymentViewController.dismiss(animated: true, completion: nil)
        //print(completedPayment)
        
        let transactionId = (completedPayment.confirmation["response"] as! [AnyHashable: Any])["id"] as! String
        
        print(transactionId)
        
        send_order(transactionId)
    }
    
}



extension PaymentViewController: STPPaymentCardTextFieldDelegate {
    
    func paymentCardTextFieldDidChange(_ textField: STPPaymentCardTextField) {
        
        if textField.isValid {
            //getTokenButton.isHidden = false
        }
        else {
            //getTokenButton.isHidden = true
        }
    }
    
    func paymentCardTextFieldDidEndEditingCVC(_ textField: STPPaymentCardTextField) {
        if textField.isValid {
            //getTokenButton.isHidden = false
        }
        else {
            //getTokenButton.isHidden = true
        }
    }
    
}

