//
//  ViewPhotosViewController.swift
//  Kaali
//
//  Created by Yin on 06/03/2018.
//  Copyright © 2018 Yin. All rights reserved.
//

import UIKit
import SKPhotoBrowser
import SDWebImage

class ViewPhotosViewController: BaseViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, SKPhotoBrowserDelegate {
    
    @IBOutlet weak var cvPhotoGrid: UICollectionView!
    @IBOutlet weak var btnExport: UIButton!

    var images = [SKPhotoProtocol]()
    
    var isSelectMode: Bool = false
    var screenWidth: CGFloat = 0.0
    var selectedIndex: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SKPhotoBrowserOptions.displayAction = true
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //self.navigationController?.isNavigationBarHidden = true
        
        UIApplication.shared.statusBarView?.backgroundColor = Const.mainColor
        
        get_photos()
        
        screenWidth = self.view.frame.size.width
        btnExport.isHidden = true
        isSelectMode = false
        cvPhotoGrid.frame.size.height = self.view.frame.size.height - 64
        cvPhotoGrid.reloadData()
        //self.images = self.createWebPhotos()
        isPaid = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func get_photos() {
        
        self.showLoadingView()
        
        ApiRequest.get_photos { (message, photos) in
            
            if message == Const.SUCCESS {
                self.hideLoadingView()
                
                event_photos = photos
                
                self.images.removeAll()
                
                for photoModel in event_photos {
                    if currentUser?.user_id == currentEvent?.event_userid {
                        let photo = SKPhoto.photoWithImageURL(photoModel.photo_url)
                        photo.shouldCachePhotoURLImage = true // you can use image cache by true(NSCache)
                        self.images.append(photo)
                    } else {
                            if photoModel.is_approved == 0 {
                            let photo = SKPhoto.photoWithImage(UIImage(named: "black")!)
                            photo.shouldCachePhotoURLImage = true // you can use image cache by true(NSCache)
                            photo.caption = "Awating approval"
                            self.images.append(photo)
                        } else {
                            let photo = SKPhoto.photoWithImageURL(photoModel.photo_url)
                            photo.shouldCachePhotoURLImage = true // you can use image cache by true(NSCache)
                            self.images.append(photo)
                        }
                    }
                }
                self.cvPhotoGrid.reloadData()
            } else {
                self.showAlertDialog(title: Const.ERROR, message: message, positive: Const.OK, negative: nil)
            }
        }
    }

    
    @IBAction func onSelect(_ sender: Any) {
        
        isSelectMode = !isSelectMode
        
        if isSelectMode {
            
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                self.cvPhotoGrid.frame.size.height = self.view.frame.size.height - 64 - 60// export button height shows
                self.view.layoutIfNeeded()
            })
            self.perform(#selector(self.showButton), with: nil, afterDelay: 0.3)
        } else {
            
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                self.cvPhotoGrid.frame.size.height = self.view.frame.size.height - 64// export button height shows
                self.view.layoutIfNeeded()
            })
            self.btnExport.isHidden = true
        }
        
        cvPhotoGrid.reloadData()
        
    }
    
    @objc func showButton() {
        
        self.btnExport.isHidden = false
        
    }
    
    @IBAction func onExport(_ sender: Any) {
        
        var export_photos = [PhotoModel]()
        
        for photo in event_photos {
            if photo.isChecked && photo.is_approved == 1{
                
                let export_photo = PhotoModel()
                export_photo.event_id = currentEvent!.event_id
                export_photo.user_id = currentUser!.user_id
                export_photo.photo_id = photo.photo_id
                export_photo.comment = ""
                export_photo.photo_url = photo.photo_url
                
                export_photos.append(export_photo)
            }
        }
        
        let commentVC = self.storyboard?.instantiateViewController(withIdentifier: "PhotoCommentViewController") as! PhotoCommentViewController
        commentVC.export_photos = export_photos
        self.present(commentVC, animated: true, completion: nil)
    }
    
    //MARK: - PhotoCollectionView DataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return event_photos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoCell", for: indexPath) as! PhotoCell
        cell.imvPhoto.sd_setImage(with: URL(string: event_photos[indexPath.row].photo_url), placeholderImage: #imageLiteral(resourceName: "placeholder"))
        if event_photos[indexPath.row].is_approved == 1 {
            cell.hideView.isHidden = true
        } else {
            cell.hideView.isHidden = false
        }
        
        if isSelectMode {
            cell.imvCheck.isHidden = false
            
            if event_photos[indexPath.row].isChecked && event_photos[indexPath.row].is_approved == 1 {
                cell.imvCheck.image = #imageLiteral(resourceName: "confirmButton")
            } else {
                cell.imvCheck.image = nil
            }
        } else {
            cell.imvCheck.isHidden = true
        }
        return cell
    }    

    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize.init(width: screenWidth/4.0 - 3, height: screenWidth/4.0 - 3)
    }
    
    //MARK:- CollectionView select sell
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        selectedIndex = indexPath.row
        
        if !isSelectMode {
            
            let browser = SKPhotoBrowser(photos: self.images, initialPageIndex: indexPath.row)
                
            if currentUser?.user_id == currentEvent?.event_userid && event_photos[indexPath.row].is_approved == 0 {
                
                let approveButton = UIButton()
                approveButton.frame = CGRect(x: self.view.frame.width / 2 - 100, y: 0, width: 50, height: 50)
                approveButton.backgroundColor = UIColor.clear
                approveButton.setBackgroundImage(UIImage(named: "confirmButton"), for: .normal)
                approveButton.addTarget(self, action: #selector(approveAction), for: .touchUpInside)
                
                let closeButton = UIButton()
                closeButton.frame = CGRect(x: self.view.frame.width / 2, y: 0, width: 50, height: 50)
                closeButton.backgroundColor = UIColor.clear
                closeButton.setBackgroundImage(UIImage(named: "retakeButton"), for: .normal)
                
                let vistaNombrePrecio: UIView = UIView()
                vistaNombrePrecio.backgroundColor = UIColor.clear
                vistaNombrePrecio.translatesAutoresizingMaskIntoConstraints = false
                
                vistaNombrePrecio.addSubview(approveButton)
                vistaNombrePrecio.addSubview(closeButton)
                browser.view.addSubview(vistaNombrePrecio)
                
                let margins = browser.view.layoutMarginsGuide
                vistaNombrePrecio.trailingAnchor.constraint(equalTo: (margins.trailingAnchor)).isActive = true
                vistaNombrePrecio.leadingAnchor.constraint(equalTo: (margins.leadingAnchor)).isActive = true
                vistaNombrePrecio.bottomAnchor.constraint(equalTo: (margins.bottomAnchor), constant: -100).isActive = true
                vistaNombrePrecio.heightAnchor.constraint(equalToConstant: 50.0).isActive = true
                
                browser.delegate = self
                present(browser, animated: true, completion: {})
                
            } /*else if images.count > 0 && event_photos[selectedIndex].is_approved == 1 {
                browser.delegate = self
                present(browser, animated: true, completion: {})
            } else {
                self.showToast(Const.MSG_AWATING)
            }*/
            else {
                browser.delegate = self
                present(browser, animated: true, completion: {})
            }
        } else {
            event_photos[indexPath.row].isChecked = !event_photos[indexPath.row].isChecked
            cvPhotoGrid.reloadData()
        }
    }
    
    @objc func approveAction(sender: UIButton!) {
        
        self.showLoadingView()
        
        ApiRequest.approve_photo(photo_id: event_photos[selectedIndex].photo_id) { (message) in
            
            self.hideLoadingView()
            
            if message == Const.SUCCESS {
                self.showToast(message)
            } else {
                self.showAlertDialog(title: Const.APP_NAME, message: message, positive: Const.OK, negative: nil)
            }
        }
        
    }
    
    //MARK: - CollectionViewDelegate FlowLayoout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
}

// MARK: - SKPhotoBrowserDelegate

extension ViewPhotosViewController {
    
    func didDismissAtPageIndex(_ index: Int) {
    }
    
    func didDismissActionSheetWithButtonIndex(_ buttonIndex: Int, photoIndex: Int) {
    }
    
    func removePhoto(index: Int, reload: (() -> Void)) {
        SKCache.sharedCache.removeImageForKey("somekey")
        reload()
    }
    
//    func didShowPhotoAtIndex(_ index: Int) {
//        cvPhotoGrid.visibleCells.forEach({$0.isHidden = false})
//        cvPhotoGrid.cellForItem(at: IndexPath(item: index, section: 0))?.isHidden = true
//    }
//
//    func willDismissAtPageIndex(_ index: Int) {
//        cvPhotoGrid.visibleCells.forEach({$0.isHidden = false})
//        cvPhotoGrid.cellForItem(at: IndexPath(item: index, section: 0))?.isHidden = true
//    }
//
//    func willShowActionSheet(_ photoIndex: Int) {
//        // do some handle if you need
//    }
//
//    func didDismissAtPageIndex(_ index: Int) {
//        cvPhotoGrid.cellForItem(at: IndexPath(item: index, section: 0))?.isHidden = false
//    }
//
//    func didDismissActionSheetWithButtonIndex(_ buttonIndex: Int, photoIndex: Int) {
//        // handle dismissing custom actions
//    }
//
//    func removePhoto(index: Int, reload: (() -> Void)) {
//        SKCache.sharedCache.removeImageForKey("somekey")
//        reload()
//    }
//
//    func viewForPhoto(_ browser: SKPhotoBrowser, index: Int) -> UIView? {
//        return cvPhotoGrid.cellForItem(at: IndexPath(item: index, section: 0))
//    }
}

// MARK: - private

private extension ViewPhotosViewController {
    func createWebPhotos() -> [SKPhotoProtocol] {
        return (0..<event_photos.count).map { (i: Int) -> SKPhotoProtocol in
            let photo = SKPhoto.photoWithImageURL(event_photos[i].photo_url, holder: #imageLiteral(resourceName: "placeholder"))
            
            photo.caption = caption[i%10]
            photo.shouldCachePhotoURLImage = true
            return photo
        }
    }
    

}

class CustomImageCache: SKImageCacheable {
    
    var cache: SDImageCache
    
    init() {
        let cache = SDImageCache(namespace: "com.suzuki.custom.cache")
        self.cache = cache
    }
    
    func imageForKey(_ key: String) -> UIImage? {
        guard let image = cache.imageFromDiskCache(forKey: key) else { return nil }
        
        return image
    }
    
    func setImage(_ image: UIImage, forKey key: String) {
        cache.store(image, forKey: key)
    }
    
    func removeImageForKey(_ key: String) {
    }
    
    func removeAllImages() {
        
    }
}

var caption = ["","","","","","","","","","","",""]

