//
//  LoginViewController.swift
//  Kaali
//
//  Created by Yin on 08/03/2018.
//  Copyright © 2018 Yin. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import SwiftyJSON

class FBIntroViewController: BaseViewController {
    
    @IBOutlet weak var tfUserName: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    
    var user = UserModel()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        initView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)        
    }
    
    func initView() {
        
        UIApplication.shared.statusBarView?.backgroundColor = UIColor.clear
        self.navigationController?.isNavigationBarHidden = true
        
        let userid = UserDefaults.standard.integer(forKey: Const.KEY_USERID)
        if userid > 0 {
            user = getCurrentUser()
            if user.login_type == 0 {
                self.doLogin()
            } else {
                tfUserName.text = user.user_name
                tfPassword.text = user.password
                self.doLoginUser()
            }
        }
    }
    

    
    @IBAction func fbLogin(_ sender: Any) {
        
        //for test
        //gotoEventLogin()
       
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions: ["public_profile", "email", "user_friends"], from: self) { (result, error) in
            if (error == nil){
                let fbloginresult : FBSDKLoginManagerLoginResult = result!
                if fbloginresult.grantedPermissions != nil {
                    if(fbloginresult.grantedPermissions.contains("email"))
                    {
                        self.getFBUserData()
                        
                        fbLoginManager.logOut()
                    }
                }
            }
        }
    }
    
    @IBAction func loginAction(_ sender: Any) {
        
        if isValid() {
            self.doLoginUser()
        }
    }
    
    @IBAction func gotoCreateAction(_ sender: Any) {
        
        let registerVC = self.storyboard?.instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController
        self.navigationController?.pushViewController(registerVC, animated: true)
    }
    func isValid() -> Bool {
        
        if tfUserName.text?.count == 0 {
            self.showAlertDialog(title: Const.ERROR, message: Const.INPUT_USERNAME, positive: Const.OK, negative: nil)
            return false
        }
        
        if tfPassword.text?.count == 0 {
            self.showAlertDialog(title: Const.ERROR, message: Const.INPUT_PASSWORD, positive: Const.OK, negative: nil)
            return false
        }
        
        return true
    }
    
    func getFBUserData(){
        
        if((FBSDKAccessToken.current()) != nil) {
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, picture.type(large), gender, email, first_name, last_name"]).start(completionHandler: { (connection, result, error) -> Void in
                
                if (error == nil) {
                    
                    let jsonResponse = JSON(result!)
                    
                    print("jsonResponse", jsonResponse)
                    
                    self.user.user_name = jsonResponse["id"].stringValue
                    self.user.email = jsonResponse["email"].string ?? String(format: "%@@facebook.com", self.user.user_name)
                    self.user.first_name = jsonResponse["first_name"].string ?? "unknown"
                    self.user.last_name = jsonResponse["last_name"].string ?? "unknown"
                    //let photo = "https://graph.facebook.com/" + id + "/picture?type=large"
                    
                    print("FB result", self.user.user_name, self.user.email, self.user.first_name, self.user.last_name)
                    
                    self.doLogin()
                    
                } else {
                    // TODO :  Exeption  ****
                    print(error!)
                }
            })
        } else {
            print("token is null")
        }
    }
    
    func doLogin() {
        
        self.showLoadingView()
        
        ApiRequest.fb_login(user: user) { (message) in
            
            self.hideLoadingView()
            if message != Const.ERROR_CONNECT {
                self.gotoEventList()
            } else {
                self.showAlertDialog(title: Const.ERROR, message: message, positive: Const.OK, negative: nil)
            }
        }
    }
    
    func doLoginUser() {
        
        let user_name = tfUserName.text!
        let password = tfPassword.text!
        
        self.showLoadingView()
        
        ApiRequest.login_user(user_name: user_name, password: password) { (message) in
            
            self.hideLoadingView()
            
            if message == Const.LOGIN_SUCCESS {
                self.gotoEventList()
            } else {
                self.showAlertDialog(title: Const.APP_NAME, message: message, positive: Const.OK, negative: nil)
            }
        }
    }
    
    func gotoEventList() {
        let eventListVC = self.storyboard?.instantiateViewController(withIdentifier: "EventListViewController") as! EventListViewController
        self.navigationController?.pushViewController(eventListVC, animated: true)
    }


}
