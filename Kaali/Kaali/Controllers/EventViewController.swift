//
//  EventViewController.swift
//  Kaali
//
//  Created by Yin on 06/03/2018.
//  Copyright © 2018 Yin. All rights reserved.
//

import UIKit
import MessageUI

class EventViewController: BaseViewController, MFMessageComposeViewControllerDelegate  {

    @IBOutlet weak var lblEventName: UILabel!
    @IBOutlet weak var lblEventID: UILabel!
    @IBOutlet weak var lblEventPwd: UILabel!
    @IBOutlet weak var btnShare: UIButton!
    
    var shareText = ""
    var shareTitle = "My Event in Kaali"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        shareText = "I made an event on the Kaali app\nEvent ID : " + currentEvent!.event_id +
            "\nEvent Password : " + currentEvent!.event_password +
        "\n\nHere is a link to the app : \nhttps://itunes.apple.com/us/app/kaali/id1374827332?ls=1&mt=8"
        
        shareTitle = "My Event in Kaali"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //self.navigationController?.isNavigationBarHidden = true
        
        UIApplication.shared.statusBarView?.backgroundColor = Const.mainColor
        
        initView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func initView() {
        
        lblEventName.text = currentEvent?.event_name
        lblEventID.text = "Event ID : " + currentEvent!.event_id
        lblEventPwd.text = "Password : " + currentEvent!.event_password
        
        if currentEvent?.event_userid == currentUser?.user_id {
            btnShare.isHidden = false
        } else {
            btnShare.isHidden = true
        }

    }
    
    @IBAction func shareAction(_ sender: Any) {
        
        let alert = UIAlertController(title: "Share with", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Email", style: .default, handler: { _ in
            self.shareEmail()
        }))
        
        alert.addAction(UIAlertAction(title: "Text", style: .default, handler: { _ in
            self.sendMessage()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = sender as! UIButton
            alert.popoverPresentationController?.sourceRect = (sender as AnyObject).bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        
        self.present(alert, animated: true, completion: nil)        
    }
    
    func shareEmail() {
        
        // set up activity view controller
        let textToShare = [ shareText ]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.title = shareTitle
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    func sendMessage() {
        if (MFMessageComposeViewController.canSendText()) {
            let controller = MFMessageComposeViewController()
            controller.title = shareTitle
            controller.subject = shareTitle
            controller.body = shareText
            controller.messageComposeDelegate = self
            self.present(controller, animated: true, completion: nil)
        }
    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        //... handle sms screen actions
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    @IBAction func backAction(_ sender: Any) {
        let eventListNav = self.storyboard?.instantiateViewController(withIdentifier: "EventListNav") as! UINavigationController
        UIApplication.shared.keyWindow?.rootViewController = eventListNav
    }
    
    @IBAction func doLogout(_ sender: Any) {

        let user = UserModel()
        UserDefaults.standard.set(user.email, forKey: Const.KEY_USER_EMAIL)
        UserDefaults.standard.set(user.user_name, forKey: Const.KEY_USER_USERNAME)
        UserDefaults.standard.set(user.first_name, forKey: Const.KEY_USER_FNAME)
        UserDefaults.standard.set(user.last_name, forKey: Const.KEY_USER_LNAME)
        UserDefaults.standard.set(user.user_id, forKey: Const.KEY_USERID)
        
        let loginNavVC = self.storyboard?.instantiateViewController(withIdentifier: "loginNav") as! UINavigationController
        UIApplication.shared.keyWindow?.rootViewController = loginNavVC
    }
    
}




