//
//  EventListViewController.swift
//  Kaali
//
//  Created by Yin on 2018/10/4.
//  Copyright © 2018 Yin. All rights reserved.
//

import UIKit

class EventListViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tblEventList: UITableView!
    
    var eventList = [EventModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        tblEventList.tableFooterView = UIView()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        getEventList()
    }
    
    @IBAction func addEventAction(_ sender: Any) {
        
        let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.navigationController?.pushViewController(loginVC, animated: true)
    }
  
    func getEventList() {
        
        eventList.removeAll()
        
        self.showLoadingView()
        
        ApiRequest.get_event { (result_code, event_list) in
            
            self.hideLoadingView()
            
            if result_code == Const.CODE_SUCCESS {
                self.eventList = event_list
            } else {
                self.showAlertDialog(title: Const.APP_NAME, message: Const.ERROR_CONNECT, positive: Const.OK, negative: nil)
            }
            
            self.tblEventList.reloadData()
        }
    }
    
    // MARK: - TableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return eventList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell(style: .default, reuseIdentifier: "EventListCell")
        cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
        cell.textLabel?.text = eventList[indexPath.row].event_name
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        currentEvent = eventList[indexPath.row]
        gotoMain()
    }
    
    func gotoMain() {
        
        let mainPageVC = self.storyboard?.instantiateViewController(withIdentifier: "PageViewController") as! PageViewController
        UIApplication.shared.keyWindow?.rootViewController = mainPageVC
    }

}
