//
//  PhotoCommentViewController.swift
//  Kaali
//
//  Created by Yin on 10/03/2018.
//  Copyright © 2018 Yin. All rights reserved.
//

import UIKit
import GrowingTextView
import SDWebImage

class PhotoCommentViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tblComment: UITableView!
    //var images = [(UIImage, String?)]()
    
    var export_photos = [PhotoModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        UIApplication.shared.statusBarView?.backgroundColor = Const.mainColor
        tblComment.tableFooterView = UIView()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        tblComment.rowHeight = UITableViewAutomaticDimension
        tblComment.estimatedRowHeight = self.view.bounds.width + 60
        
        if isPaid {
            self.dismiss(animated: false, completion: nil)
        }
    }
    
    @IBAction func onCancel(_ sender: Any) {
    
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onContinue() {
        
        let payVC = self.storyboard?.instantiateViewController(withIdentifier: "PaymentViewController") as! PaymentViewController
        payVC.order_photos = export_photos
        self.present(payVC, animated: true, completion: nil)
    }
    
    //MARK:- TableViewDatasource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return export_photos.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PhotoCommentCell") as! PhotoCommentCell
        //cell.textViewGrowingDelegate = self
        let imageInfo = export_photos[indexPath.row]
        if let image = SDImageCache.shared().imageFromDiskCache(forKey: imageInfo.photo_url) {
            cell.imageHeightConstraint.constant = UIScreen.main.bounds.width * image.size.height / image.size.width
            cell.imvphoto.image = image
            cell.imageSize = image.size
        }
        else  {
            cell.imvphoto.sd_setImage(with: URL(string: imageInfo.photo_url), placeholderImage: #imageLiteral(resourceName: "placeholder")) {
                image, url, cache, error in
                if let image = image {
                    cell.imageHeightConstraint.constant = UIScreen.main.bounds.width * image.size.height / image.size.width
                    
                    cell.imageSize = image.size
                    cell.frame.size.height = cell.commentTextHeightConstraint.constant + cell.imageHeightConstraint.constant + 20
                    cell.layoutIfNeeded()
                    tableView.layoutIfNeeded()
                }
            }
            
        }
        //cell.indexPath = indexPath
        cell.txvComment.text = imageInfo.comment
        cell.commentChangeHandler = { [weak self] cell, comment in
            if let idx = tableView.indexPath(for: cell) {
                self?.export_photos[idx.row].comment = comment!
            }
        }
        return cell
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.view.endEditing(true)
    }
}


